from flask import Blueprint, render_template, url_for, request, redirect, flash, current_app
import requests
import polyline
import math

import sys
import pprint


main = Blueprint('main', __name__)


@main.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        activities = {}
        for form_id, value in request.form.items():
            if not value or form_id == 'submit':
                continue

            form_index = form_id[-1]
            if form_index not in activities:
                activities[form_index] = {'activity_type': '', 'activity_id': ''}
            if form_id[:2] == 'id':
                activities[form_index]['activity_id'] = value
            elif form_id[:4] == 'type':
                activities[form_index]['activity_type'] = value

        url = '/compare/'
        for index, values in activities.items():
            url += '{}/{}/'.format(values['activity_type'], values['activity_id'])

        return redirect(url)
    else:
        return render_template('index.html')


@main.route('/token_exchange')
def token_exchange():
    payload = {
        'client_id': current_app.config['STRAVA_API_CLIENT_ID'],
        'client_secret': current_app.config['STRAVA_API_CLIENT_SECRET'],
        'code': request.args.get('code')
    }
    r = requests.post('https://www.strava.com/oauth/token', data=payload)

    return r.json()



@main.route('/compare/<path:varargs>')
def compare(varargs=None):
    varargs = varargs.split('/')
    stream_ids = {}
    for i, vararg in enumerate(varargs):
        if i % 2 == 0:
            continue
        stream_ids[varargs[i]] = varargs[i-1]

    strava_streams = _get_multiple_streams(stream_ids)
    if 'error' in strava_streams:
        flash(strava_streams['error'])
        return render_template('index.html')

    google_array = _multiple_output_strava_stream_to_google_dt_array(strava_streams)

    return render_template('compare.html', google_array=google_array)


def _multiple_output_strava_stream_to_google_dt_array(strava_streams):
    num_streams = len(strava_streams)
    name_mapping = []
    stream_distance = {}
    stream_altitude = {}
    output_string = "[['Distance',"
    for stream_id, values in strava_streams.items():
        activity_info = _get_strava_activity_info(stream_id, values['type'])
        stream_name = activity_info['name']
        name_mapping.append(stream_name)
        for stream in values['stream']:
            if stream['type'] == 'distance':
                stream_distance[stream_name] = stream['data']
            elif stream['type'] == 'altitude':
                stream_altitude[stream_name] = stream['data']
        output_string += " '{}',".format(stream_name)
    output_string += "],"

    combined_profiles = {}
    for stream_name, values in stream_distance.items():
        for distance in values:
            if distance not in combined_profiles:
                combined_profiles[distance] = ['null'] * num_streams
            combined_profiles[distance][name_mapping.index(stream_name)] = stream_altitude[stream_name][stream_distance[stream_name].index(distance)]

    for distance_index in sorted(combined_profiles):
        output_string += "[{},".format(distance_index)
        for i in range(num_streams):
            output_string += "{},".format(combined_profiles[distance_index][i])
        output_string += "],"
    output_string += "]"

    return output_string


def _get_multiple_streams(strava_stream_ids):
    streams = {}

    for id, type in strava_stream_ids.items():
        streams[id] = {'type': type}
        if type == 'routes':
            # Need to get data from Google instead
            # route_info = {
            #     'athlete': {
            #         'badge_type_id': 1,
            #         'city': 'Lindau',
            #         'country': 'Germany',
            #         'created_at': '2015-03-15T17:27:40Z',
            #         'email': 'jmorgancampbell@gmail.com',
            #         'firstname': 'Morgan',
            #         'follower': None,
            #         'friend': None,
            #         'id': 8253960,
            #         'lastname': 'Campbell',
            #         'premium': True,
            #         'profile': 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/8253960/2680259/1/large.jpg',
            #         'profile_medium': 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/8253960/2680259/1/medium.jpg',
            #         'resource_state': 2,
            #         'sex': 'M',
            #         'state': 'Bavaria',
            #         'updated_at': '2017-09-10T11:39:21Z',
            #         'username': 'jmorganc'
            #     },
            #     'created_at': '2017-09-03T08:21:51Z',
            #     'description': '',
            #     'distance': 7508.79713117768,
            #     'elevation_gain': 474.79361545554787,
            #     'estimated_moving_time': 7174,
            #     'id': 10433757,
            #     'map': {
            #         'id': 'r10433757',
            #         'polyline': 'utw~Gsjqz@D}@KEOQCKkAaCOIUCYBs@Z[Hm@DUA}@YM?kBj@_@TmB|Bu@l@k@Ne@XkAZa@V{BdCaAzAc@bAo@~@mAvAcCtB[`@o@|BGb@CbDMfAGnAUvBGTe@l@DV^jAAPEJKAy@]c@GcBc@YMe@g@QEQQ]yAOwAOu@AOQ]e@g@EIKg@}@[]EGSIKqAbBWEc@UEGCUGO?KKUPiA?QCMPo@Ce@FgAOaA@]GY?OFW@k@\\y@Jy@Aa@@OV}@f@c@BKDaA\\uCx@qCOeEFkAaAaEZsJJcBs@aOVw@O}@JkCZmAMqC^{A?qDDkBYe@@gCh@}@?aDSwAdAoIQeI`@t@RT^XlAdBr@tBf@lAj@hBV\\Nr@rBbHJf@APQNKTGZC\\BdAOv@Bx@?~@H@l@_BRULCX@JDDFD\\Rr@IRm@B]RGPMfA@`@D\\h@^JRLrA@\\Iz@Sl@KLK?CI?_@K]m@o@GC_@Fg@`@YH]XE@CAGOSkAEHGfASjAK\\ChBMdAIjBBnCE~@FdB?hCJ`AAtAF`@D|@JVV^Td@Dl@@~@f@t@DJ@LCfBIl@S`AIfAKrFL~AHDDABER_BDqAHs@HoATi@Va@Re@^m@HWf@sCNi@FcCBBBzBEv@e@zDUp@Mr@Q~B@T`@zCFt@EtAP`EMpANt@XWNJDNAfAJ?b@\\v@?h@[j@d@|AgBn@_Ab@cA`A{AzBeC`@WjA[d@Yj@Ot@m@lB}B^UjBk@L?|@XR@n@EZIr@[XCTBNHHL`ArBBJNPJDE|@',
            #         'resource_state': 3,
            #         'summary_polyline': 'utw~Gsjqz@uBmFoGNoNjI_OtQuCxS\\`CqHyCiCoJaGA?}OjE}RiAsMf@wMs@aOxBw}@bDjEbHbTg@pKxAwBdA`BkBdCjAfE[fC}AmBeCnA_@}AiAvM\\vTpBzGi@rRpFmX}AtQx@fSjFrBnJmMnNkInGOtBlF'
            #     },
            #     'name': 'Tschalengagratweg',
            #     'private': False,
            #     'resource_state': 3,
            #     'segments': [],
            #     'starred': False,
            #     'sub_type': 4,
            #     'timestamp': 1504426911,
            #     'type': 2,
            #     'updated_at': '2017-09-03T08:21:51Z'
            # }
            route_info = _get_strava_activity_info(id, type)
            streams[id]['stream'] = _build_stream_format_from_polyline(route_info['map']['polyline'])
        else:
            streams[id]['stream'] = _get_strava_stream(id, type)

        if 'error' in streams[id]['stream']:
            return streams[id]['stream']

    return streams


def _build_stream_format_from_polyline(strava_polyline):
    coordinate_list = polyline.decode(strava_polyline)
    # google_elevations = [
    #     {
    #         'elevation': 1178.27392578125,
    #         'location': {
    #             'lat': 47.14843,
    #             'lng': 9.76058
    #         },
    #         'resolution': 152.7032318115234
    #     }
    # ]
    google_elevations = _get_google_elevations(coordinate_list)
    distances = _get_distance(google_elevations)

    # streams = {
    #     '1154876561': {
    #         'stream': [
    #             {
    #                 'data': [
    #                     0.1,
    #                     0.2
    #                 ],
    #                 'original_size': 10045,
    #                 'resolution': 'high',
    #                 'series_type': 'distance',
    #                 'type': 'distance'
    #             },
    #             {
    #                 'data': [
    #                     741.0,
    #                     741.0
    #                 ],
    #                 'original_size': 10045,
    #                 'resolution': 'high',
    #                 'series_type': 'distance',
    #                 'type': 'altitude'
    #             }
    #         ],
    #         'type': 'activities'
    #     }
    # }
    stream_format = [
        {
            'data': [],
            'type': 'distance'
        },
        {
            'data': [],
            'type': 'altitude'
        }
    ]
    for datapoint in google_elevations:
        # datapoint = {
        #     'resolution': 152.7032318115234,
        #     'distance': 0,
        #     'elevation': 1178.27392578125,
        #     'location': {
        #         'lat': 47.14843,
        #         'lng': 9.76058
        #     }
        # }
        stream_format[0]['data'].append(datapoint['distance'])
        stream_format[1]['data'].append(datapoint['elevation'])

    return stream_format


def _get_google_elevations(coordinate_list):
    google_elevations = []

    for url in _get_query_url_iter(coordinate_list):
        r = requests.get(url)
        google_elevations.extend(r.json()['results'])

    return google_elevations


def _get_query_url_iter(coordinate_list):
    index_start = 0
    index_end = current_app.config['GOOGLE_LOCATIONS_LIMIT']

    while coordinate_list[index_start:index_end]:
        path_string = _get_coords_string_polyline(coordinate_list[index_start:index_end])
        url_path = '{}?key={}&path={}'.format(current_app.config['GOOGLE_API_BASE_URL'], current_app.config['GOOGLE_API_KEY'], path_string)

        index_start = index_end
        index_end += current_app.config['GOOGLE_LOCATIONS_LIMIT']

        yield url_path


def _get_coords_string_polyline(coordinate_list):
    coords_string = polyline.encode(coordinate_list)

    return 'enc:{}&samples={}'.format(coords_string, len(coordinate_list))


def _get_distance(google_elevations):
    total_distance_m = 0

    for i, coords in enumerate(google_elevations):
        (lat1, lon1) = (coords['location']['lat'], coords['location']['lng'])
        google_elevations[i]['distance'] = total_distance_m
        try:
            (lat2, lon2) = (google_elevations[i + 1]['location']['lat'], google_elevations[i + 1]['location']['lng'])
        except IndexError:
            # End of the coordinate list
            break

        total_distance_m += _get_distance_between_coordinates(lat1, lon1, lat2, lon2)

    return total_distance_m


def _get_distance_between_coordinates(lat1, lon1, lat2, lon2):
    R = 6371
    dLat = _degrees_to_radians(lat2 - lat1)
    dLon = _degrees_to_radians(lon2 - lon1)
    a = math.sin(dLat / 2) * math.sin(dLat / 2) + \
        math.cos(_degrees_to_radians(lat1)) * math.cos(_degrees_to_radians(lat2)) * \
        math.sin(dLon / 2) * math.sin(dLon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = R * c

    return d * 1000


def _degrees_to_radians(deg):
    return deg * (math.pi / 180)


def _get_strava_stream(id, type):
    STREAM_TYPES = ['altitude', 'distance']
    url = '{}/{}/{}/streams/{}'.format(current_app.config['STRAVA_API_BASE_URL'], type, id, ','.join(STREAM_TYPES))
    headers = {'Authorization': 'Bearer {}'.format(current_app.config['STRAVA_API_KEY'])}

    r = requests.get(url, headers=headers)
    if r.status_code == 401:
        return {'error': r.json()['message']}

    return r.json()


def _get_strava_activity_info(id, type):
    url = '{}/{}/{}'.format(current_app.config['STRAVA_API_BASE_URL'], type, id)
    headers = {'Authorization': 'Bearer {}'.format(current_app.config['STRAVA_API_KEY'])}

    r = requests.get(url, headers=headers)

    return r.json()