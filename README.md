# Elevation Profile
A simple way to compare elevation profiles from Strava or GPX files.

## TODO
- Parse single GPX files
- Parse GPX files with multiple tracks
- Parse GPX files with elevation (Strava)
- Label total distance, elevation gained and lost
- Change the zeroing of the zeroed chart so that the lowest point of any profile is 0 (nothing should go below 0)
- Documentation
- Cache activity ID streams for x number of days to save on API calls

## Ideas
- Set up GitLab CI/Pipelines
- Wiki, or will Readme suffice?
- Google Docs, add Strava (route|segment|activity) ID, automatically parse and update a graph in the doc.
- Use Stravalib? http://pythonhosted.org/stravalib/

## Notes
Create instance configuration file in elevation_profile/instance/config.py
### Contents
STRAVA_API_BASE_URL = 'https://www.strava.com/api/v3'
STRAVA_API_KEY = ''
STRAVA_API_CLIENT_ID =
STRAVA_API_CLIENT_SECRET = ''

GOOGLE_API_KEY = ''
GOOGLE_LOCATIONS_LIMIT = 512
GOOGLE_API_BASE_URL = 'https://maps.googleapis.com/maps/api/elevation/json'